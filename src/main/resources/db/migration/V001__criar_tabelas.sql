CREATE TABLE public.usuario (
    codigo BIGSERIAL,
    nome VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    senha VARCHAR(120) NOT NULL,
    ativo BOOLEAN DEFAULT true NOT NULL,
    data_nascimento DATE,
    CONSTRAINT usuario_pkey PRIMARY KEY (codigo)
);

CREATE TABLE public.grupo (
    codigo BIGSERIAL,
    nome VARCHAR(50) NOT NULL,
    CONSTRAINT grupo_pkey PRIMARY KEY (codigo)
);

CREATE TABLE public.permissao (
    codigo BIGSERIAL,
    nome VARCHAR(50) NOT NULL,
    CONSTRAINT permissao_pkey PRIMARY KEY (codigo)
);

CREATE TABLE public.usuario_grupo (
    codigo_usuario INT8 NOT NULL,
    codigo_grupo INT8 NOT NULL,
    PRIMARY KEY (codigo_usuario, codigo_grupo),
    FOREIGN KEY (codigo_usuario) REFERENCES public.usuario(codigo),
    FOREIGN KEY (codigo_grupo) REFERENCES public.grupo(codigo)
);

CREATE TABLE public.grupo_permissao (
    codigo_grupo INT8 NOT NULL,
    codigo_permissao INT8 NOT NULL,
    PRIMARY KEY (codigo_grupo, codigo_permissao),
    FOREIGN KEY (codigo_grupo) REFERENCES public.grupo(codigo),
    FOREIGN KEY (codigo_permissao) REFERENCES public.permissao(codigo)
);


CREATE TABLE public.categoria (
    codigo BIGSERIAL,
    nome VARCHAR(50) NOT NULL,
    CONSTRAINT categoria_pkey PRIMARY KEY (codigo)
);

CREATE TABLE public.produto (
    codigo BIGSERIAL,
    sku VARCHAR(50) NOT NULL,
    nome VARCHAR(80) NOT NULL,
    descricao TEXT NOT NULL,
    valor DECIMAL(10, 2) NOT NULL,
    comissao DECIMAL(10, 2) NOT NULL,
    tipo_produto VARCHAR(50) NOT NULL,
    origem VARCHAR(50) NOT NULL,
    codigo_categoria INT8 NOT NULL,
    quantidade_estoque INTEGER,
    foto VARCHAR(100),
   	content_type VARCHAR(100),
    CONSTRAINT produto_pkey PRIMARY KEY (codigo),
    FOREIGN KEY (codigo_categoria) REFERENCES public.categoria(codigo)
);

CREATE TABLE public.estado (
    codigo BIGSERIAL,
    nome VARCHAR(50) NOT NULL,
    sigla VARCHAR(2) NOT NULL,
    CONSTRAINT estado_pkey PRIMARY KEY (codigo)
);

CREATE TABLE public.cidade (
    codigo BIGSERIAL,
    nome VARCHAR(50) NOT NULL,
    codigo_estado INT8 NOT NULL,
    CONSTRAINT cidade_pkey PRIMARY KEY (codigo),
    FOREIGN KEY (codigo_estado) REFERENCES public.estado(codigo)
);

CREATE TABLE public.cliente (
    codigo BIGSERIAL,
    nome VARCHAR(80) NOT NULL,
    tipo_pessoa VARCHAR(15) NOT NULL,
    cpf_cnpj VARCHAR(30) NOT NULL,
    telefone VARCHAR(20),
    email VARCHAR(50) NOT NULL,
    logradouro VARCHAR(50),
    numero VARCHAR(15),
    complemento VARCHAR(20),
    cep VARCHAR(15),
    codigo_cidade INT8,
    codigo_estado INT8,
    CONSTRAINT cliente_pkey PRIMARY KEY (codigo),
    FOREIGN KEY (codigo_cidade) REFERENCES public.cidade(codigo),
    FOREIGN KEY (codigo_estado) REFERENCES public.estado(codigo)
);

CREATE TABLE public.venda (
    codigo BIGSERIAL,
    data_criacao DATE NOT NULL,
    valor_frete DECIMAL(10,2),
    valor_desconto DECIMAL(10,2),
    valor_total DECIMAL(10,2) NOT NULL,
    status VARCHAR(30) NOT NULL,
    observacao VARCHAR(200),
    data_hora_entrega DATE,
    codigo_cliente INT8 NOT NULL,
    codigo_usuario INT8 NOT NULL,
    CONSTRAINT venda_pkey PRIMARY KEY (codigo),
    FOREIGN KEY (codigo_cliente) REFERENCES public.cliente(codigo),
    FOREIGN KEY (codigo_usuario) REFERENCES public.usuario(codigo)
);

CREATE TABLE public.item_venda (
    codigo BIGSERIAL,
    quantidade INTEGER NOT NULL,
    valor_unitario DECIMAL(10,2) NOT NULL,
    codigo_produto INT8 NOT NULL,
    codigo_venda INT8 NOT NULL,
    CONSTRAINT item_venda_pkey PRIMARY KEY (codigo),
    FOREIGN KEY (codigo_produto) REFERENCES public.produto(codigo),
    FOREIGN KEY (codigo_venda) REFERENCES public.venda(codigo)
);

