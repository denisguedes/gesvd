INSERT INTO public.permissao VALUES (1, 'ROLE_CADASTRAR_CIDADE');
INSERT INTO public.permissao VALUES (2, 'ROLE_CADASTRAR_USUARIO');
INSERT INTO public.permissao VALUES (3, 'ROLE_CANCELAR_VENDA');

INSERT INTO public.grupo (codigo, nome) VALUES (1, 'Administrador');
INSERT INTO public.grupo (codigo, nome) VALUES (2, 'Vendedor');

INSERT INTO public.grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 1);
INSERT INTO public.grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 2);
INSERT INTO public.grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 3);

INSERT INTO public.usuario (nome, email, senha, ativo) VALUES ('Admin', 'administrador', '$2a$10$rmaoICl3ur4AAxI2TACAH.8tKT3ZcSwe/7ha9hbe0.suwj.r7C0q2', true);

INSERT INTO public.usuario_grupo (codigo_usuario, codigo_grupo) VALUES (
	(SELECT codigo FROM usuario WHERE email = 'administrador'), 1);


INSERT INTO public.estado (codigo, nome, sigla) VALUES (1,'Acre', 'AC');
INSERT INTO public.estado (codigo, nome, sigla) VALUES (2,'Bahia', 'BA');
INSERT INTO public.estado (codigo, nome, sigla) VALUES (3,'Ceará', 'CE');
INSERT INTO public.estado (codigo, nome, sigla) VALUES (4,'Goiás', 'GO');
INSERT INTO public.estado (codigo, nome, sigla) VALUES (5,'Minas Gerais', 'MG');
INSERT INTO public.estado (codigo, nome, sigla) VALUES (6,'Santa Catarina', 'SC');
INSERT INTO public.estado (codigo, nome, sigla) VALUES (7,'São Paulo', 'SP');


INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Rio Branco', 1);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Cruzeiro do Sul', 1);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Salvador', 2);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Porto Seguro', 2);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Santana', 2);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Aquiraz', 3);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Eusébio', 3);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Fortaleza', 3);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Itaitinga', 3);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Goiânia', 4);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Itumbiara', 4);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Novo Brasil', 4);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Belo Horizonte', 5);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Uberlândia', 5);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Montes Claros', 5);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Florianópolis', 6);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Criciúma', 6);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Camboriú', 6);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Lages', 6);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('São Paulo', 7);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Ribeirão Preto', 7);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Campinas', 7);
INSERT INTO public.cidade (nome, codigo_estado) VALUES ('Santos', 6);