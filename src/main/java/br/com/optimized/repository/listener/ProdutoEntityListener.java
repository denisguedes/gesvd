package br.com.optimized.repository.listener;

import javax.persistence.PostLoad;

import org.springframework.stereotype.Component;

import br.com.optimized.model.Produto;
import br.com.optimized.storage.FotoStorage;

@Component
public class ProdutoEntityListener {

//	@Autowired
//	private FotoStorage fotoStorage;

	@PostLoad
	public void postLoad(final Produto produto) {
//		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		
		produto.setUrlFoto(FotoStorage.URL + produto.getFotoOuMock());
		produto.setUrlThumbnailFoto(FotoStorage.URL + FotoStorage.THUMBNAIL_PREFIX + produto.getFotoOuMock());
	}

}
