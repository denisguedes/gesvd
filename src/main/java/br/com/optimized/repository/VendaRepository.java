package br.com.optimized.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.model.Venda;
import br.com.optimized.repository.query.VendaRepositoryQuery;

@Repository
public interface VendaRepository extends JpaRepository<Venda, Long>, VendaRepositoryQuery {

}
