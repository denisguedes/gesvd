package br.com.optimized.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.model.Categoria;
import br.com.optimized.repository.query.CategoriaRepositoryQuery;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Long>, CategoriaRepositoryQuery {

	public Optional<Categoria> findByNomeIgnoreCase(String nome);
	
}
