package br.com.optimized.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.optimized.model.Cidade;
import br.com.optimized.repository.filter.CidadeFilter;

public interface CidadeRepositoryQuery {

	public Page<Cidade> filtrar(CidadeFilter filtro, Pageable pageable);
	
}
