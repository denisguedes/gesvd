package br.com.optimized.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.optimized.model.Categoria;
import br.com.optimized.repository.filter.CategoriaFilter;

public interface CategoriaRepositoryQuery {
	
	public Page<Categoria> filtrar(CategoriaFilter filtro, Pageable pageable);
	
}
