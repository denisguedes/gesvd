package br.com.optimized.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.optimized.model.Cliente;
import br.com.optimized.repository.filter.ClienteFilter;

public interface ClienteRepositoryQuery {

	public Page<Cliente> filtrar(ClienteFilter filtro, Pageable pageable);
	
}
