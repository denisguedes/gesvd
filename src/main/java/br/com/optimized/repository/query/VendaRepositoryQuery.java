package br.com.optimized.repository.query;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.optimized.dto.VendaMes;
import br.com.optimized.dto.VendaOrigem;
import br.com.optimized.model.Venda;
import br.com.optimized.repository.filter.VendaFilter;

public interface VendaRepositoryQuery {

	public Page<Venda> filtrar(VendaFilter filtro, Pageable pageable);
	
	public Venda buscarComItens(Long codigo);
	
	public BigDecimal valorTotalNoAno();
	public BigDecimal valorTotalNoMes();
	public BigDecimal valorTicketMedioNoAno();
	
	public List<VendaMes> totalPorMes();
	public List<VendaOrigem> totalPorOrigem();
	
}
