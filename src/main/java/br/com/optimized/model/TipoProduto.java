package br.com.optimized.model;

public enum TipoProduto {

	ACO("Aço"),
	FOLHEADO("Folheado"),
	OURO("Ouro"),
	PRATA("Prata");
	
	private String descricao;
	
	TipoProduto(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
