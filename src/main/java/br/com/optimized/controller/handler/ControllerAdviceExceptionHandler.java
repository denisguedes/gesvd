package br.com.optimized.controller.handler;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.optimized.service.exception.NomeCategoriaJaCadastradaException;

@ControllerAdvice
public class ControllerAdviceExceptionHandler {

	@ExceptionHandler(NomeCategoriaJaCadastradaException.class)
	public ResponseEntity<String> handleNomeCategoriaJaCadastradoException(NomeCategoriaJaCadastradaException e) {
		return ResponseEntity.badRequest().body(e.getMessage());
	}
	
}
