package br.com.optimized.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.optimized.controller.page.PageWrapper;
import br.com.optimized.model.Categoria;
import br.com.optimized.repository.filter.CategoriaFilter;
import br.com.optimized.service.CategoriaService;
import br.com.optimized.service.exception.ImpossivelExcluirEntidadeException;
import br.com.optimized.service.exception.NomeCategoriaJaCadastradaException;

@Controller
@RequestMapping("/categorias")
public class CategoriaController {

	@Autowired
	private CategoriaService categoriaService;
	
	@RequestMapping("/novo")
	public ModelAndView novo(Categoria entidade) {
		return new ModelAndView("categoria/CadastroCategoria");
	}
	
	@RequestMapping(value = { "/novo", "{\\d+}" }, method = RequestMethod.POST)
	public ModelAndView salvar(@Valid Categoria entidade, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(entidade);
		}
		
		try {
			categoriaService.salvar(entidade);
		} catch (NomeCategoriaJaCadastradaException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return novo(entidade);
		}
		
		attributes.addFlashAttribute("mensagem", "Categoria salvo com sucesso");
		return new ModelAndView("redirect:/categorias/novo");
	}
	
	@GetMapping
	public ModelAndView pesquisar(CategoriaFilter categoriaFilter, BindingResult result
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView("categoria/PesquisaCategorias");
		
		PageWrapper<Categoria> paginaWrapper = new PageWrapper<>(categoriaService.filtrar(categoriaFilter, pageable)
				, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		return mv;
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Categoria entidade) {
		try {
			categoriaService.excluir(entidade);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable("codigo") Categoria entidade) {
		ModelAndView mv = novo(entidade);
		mv.addObject(entidade);
		return mv;
	}
}
