package br.com.optimized.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.optimized.service.ClienteService;
import br.com.optimized.service.ProdutoService;
import br.com.optimized.service.VendaService;

@Controller
public class DashboardController {

	@Autowired
	private VendaService vendaService;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping("/")
	public ModelAndView dashboard() {
		ModelAndView mv = new ModelAndView("Dashboard");
		
		mv.addObject("vendasNoAno", vendaService.valorTotalNoAno());
		mv.addObject("vendasNoMes", vendaService.valorTotalNoMes());
		mv.addObject("ticketMedio", vendaService.valorTicketMedioNoAno());
		
		mv.addObject("valorItensEstoque", produtoService.valorItensEstoque());
		mv.addObject("totalClientes", clienteService.quantidade());
		
		return mv;
	}
	
}
