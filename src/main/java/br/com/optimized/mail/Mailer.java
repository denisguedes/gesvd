package br.com.optimized.mail;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import br.com.optimized.model.ItemVenda;
import br.com.optimized.model.Produto;
import br.com.optimized.model.Venda;
import br.com.optimized.storage.FotoStorage;

@Component
public class Mailer {
	
	private static Logger logger = LoggerFactory.getLogger(Mailer.class);

	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private TemplateEngine thymeleaf;
	
	@Autowired
	private FotoStorage fotoStorage;
	
	@Async
	public void enviar(Venda venda) {
		Context context = new Context(new Locale("pt", "BR"));
		
		context.setVariable("venda", venda);
		context.setVariable("logo", "logo");
		
		Map<String, String> fotos = new HashMap<>();
		boolean adicionarMockProduto = false;
		for (ItemVenda item : venda.getItens()) {
			Produto produto = item.getProduto();
			if (produto.temFoto()) {
				String cid = "foto-" + produto.getCodigo();
				context.setVariable(cid, cid);
				
				fotos.put(cid, produto.getFoto() + "|" + produto.getContentType());
			} else {
				adicionarMockProduto = true;
				context.setVariable("mockProduto", "mockProduto");
			}
		}
		
		try {
			String email = thymeleaf.process("mail/ResumoVenda", context);
		        
			JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
			javaMailSender.setHost("smtp.gmail.com");
			javaMailSender.setPort(587);
			javaMailSender.setProtocol("smtp");
			javaMailSender.setUsername("denisguedes23@gmail.com");
			javaMailSender.setPassword("Oliveira.091");
			javaMailSender.setDefaultEncoding("utf-8");

			Properties properties = new Properties();
			properties.setProperty("username", "denisguedes23@gmail.com");
			properties.setProperty("password", "Oliveira.091");
			properties.setProperty("mail.smtp.starttls.enable", "true");
			properties.setProperty("mail.transport.protocol", "smtp");
			javaMailSender.setJavaMailProperties(properties);
			
			MimeMessage msg = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, false);
			helper.setFrom("denisguedes23@gmail.com");
			helper.setSubject(String.format("OH Coisa Linda - Venda nº %d", venda.getCodigo()));
			helper.setText(email, true);
			helper.addTo(venda.getCliente().getEmail());
	
			
//			helper.addInline("logo", new ClassPathResource("static/images/logo-oh.jpeg"));
//			
//			if (adicionarMockProduto) {
//				helper.addInline("mockProduto", new ClassPathResource("static/images/logo-oh.jpeg"));
//			}
			
//			for (String cid : fotos.keySet()) {
//				String[] fotoContentType = fotos.get(cid).split("\\|");
//				String foto = fotoContentType[0];
//				String contentType = fotoContentType[1];
//				byte[] arrayFoto = fotoStorage.recuperarThumbnail(foto);
//				helper.addInline(cid, new ByteArrayResource(arrayFoto), contentType);
//			}
		
			javaMailSender.send(msg);
	
		} catch (MessagingException e) {
			logger.error("Erro enviando e-mail", e);
		}
	}
	
}
