package br.com.optimized.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.optimized.model.Cliente;
import br.com.optimized.repository.ClienteRepository;
import br.com.optimized.repository.filter.ClienteFilter;
import br.com.optimized.service.exception.CpfCnpjClienteJaCadastradoException;
import br.com.optimized.service.exception.ImpossivelExcluirEntidadeException;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	public Page<Cliente> filtrar(ClienteFilter filtro, Pageable pageable){
		return clienteRepository.filtrar(filtro, pageable);
	}
	
	public Long quantidade() {
		return clienteRepository.count();
	}
	
	public Optional<Cliente> findByCpfOuCnpj(String cpfOuCnpj){
		return clienteRepository.findByCpfOuCnpj(cpfOuCnpj);
	}

	public List<Cliente> findByNomeStartingWithIgnoreCase(String nome){
		return clienteRepository.findByNomeStartingWithIgnoreCase(nome);
	}
	
	@Transactional
	public void salvar(Cliente cliente) {
		Optional<Cliente> clienteExistente = clienteRepository.findByCpfOuCnpj(cliente.getCpfOuCnpjSemFormatacao());
		if (clienteExistente.isPresent() && cliente.getCodigo() == null) {
			throw new CpfCnpjClienteJaCadastradoException("CPF/CNPJ já cadastrado");
		}
		clienteRepository.save(cliente);
	}
	
	@Transactional
	public void excluir(Cliente categoria) {
		try {
			clienteRepository.delete(categoria);
			clienteRepository.flush();
		} catch (PersistenceException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar Cliente..");
		}
	}
	
}
