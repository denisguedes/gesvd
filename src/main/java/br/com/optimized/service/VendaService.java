package br.com.optimized.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.optimized.dto.VendaMes;
import br.com.optimized.dto.VendaOrigem;
import br.com.optimized.model.StatusVenda;
import br.com.optimized.model.Venda;
import br.com.optimized.repository.VendaRepository;
import br.com.optimized.repository.filter.VendaFilter;
import br.com.optimized.service.event.venda.VendaEvent;

@Service
public class VendaService {

	@Autowired
	private VendaRepository vendaRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	public Page<Venda> filtrar(VendaFilter filtro, Pageable pageable){
		return vendaRepository.filtrar(filtro, pageable);
	}
	
	public Venda buscarComItens(Long codigo) {
		return vendaRepository.buscarComItens(codigo);
	}
	
	public BigDecimal valorTotalNoAno() {
		return vendaRepository.valorTotalNoAno();
	}
	
	public BigDecimal valorTotalNoMes() {
		return vendaRepository.valorTotalNoMes();
	}
	
	public BigDecimal valorTicketMedioNoAno() {
		return vendaRepository.valorTicketMedioNoAno();
	}
	
	public List<VendaMes> totalPorMes(){
		return vendaRepository.totalPorMes();
	}
	
	public List<VendaOrigem> totalPorOrigem(){
		return vendaRepository.totalPorOrigem();
	}
	
	@Transactional
	public Venda salvar(Venda venda) {
		if (venda.isSalvarProibido()) {
			throw new RuntimeException("Usuário tentando salvar uma venda proibida");
		}
		
		if (venda.isNova()) {
			venda.setDataCriacao(LocalDateTime.now());
		} else {
			Venda vendaExistente = vendaRepository.findOne(venda.getCodigo());
			venda.setDataCriacao(vendaExistente.getDataCriacao());
		}
		
		if (venda.getDataEntrega() != null) {
			venda.setDataHoraEntrega(LocalDateTime.of(venda.getDataEntrega()
					, venda.getHorarioEntrega() != null ? venda.getHorarioEntrega() : LocalTime.NOON));
		}
		
		return vendaRepository.saveAndFlush(venda);
	}

	@Transactional
	public void emitir(Venda venda) {
		venda.setStatus(StatusVenda.EMITIDA);
		salvar(venda);
		
		publisher.publishEvent(new VendaEvent(venda));
	}

	@PreAuthorize("#venda.usuario == principal.usuario or hasRole('CANCELAR_VENDA')")
	@Transactional
	public void cancelar(Venda venda) {
		Venda vendaExistente = vendaRepository.findOne(venda.getCodigo());
		
		vendaExistente.setStatus(StatusVenda.CANCELADA);
		vendaRepository.save(vendaExistente);
	}

}
