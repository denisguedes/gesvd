package br.com.optimized.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.optimized.model.Categoria;
import br.com.optimized.repository.CategoriaRepository;
import br.com.optimized.repository.filter.CategoriaFilter;
import br.com.optimized.service.exception.ImpossivelExcluirEntidadeException;
import br.com.optimized.service.exception.NomeCategoriaJaCadastradaException;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public Page<Categoria> filtrar(CategoriaFilter filtro, Pageable pageable){
		return categoriaRepository.filtrar(filtro, pageable);
	}
	
	public List<Categoria> listar(){
		return categoriaRepository.findAll();
	}
	
	@Transactional
	public void salvar(Categoria categoria) {
		Optional<Categoria> categoriaOptional = categoriaRepository.findByNomeIgnoreCase(categoria.getNome());
		if (categoriaOptional.isPresent()) {
			throw new NomeCategoriaJaCadastradaException("Nome da categoria já cadastrado");
		}
		categoriaRepository.save(categoria);
	}
	
	@Transactional
	public void excluir(Categoria categoria) {
		try {
			categoriaRepository.delete(categoria);
			categoriaRepository.flush();
		} catch (PersistenceException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar Categoria. Já foi usada em algum produto.");
		}
	}
	
}
