package br.com.optimized.service;

import java.util.List;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.optimized.dto.ProdutoDTO;
import br.com.optimized.dto.ValorItensEstoque;
import br.com.optimized.model.Produto;
import br.com.optimized.repository.ProdutoRepository;
import br.com.optimized.repository.filter.ProdutoFilter;
import br.com.optimized.service.exception.ImpossivelExcluirEntidadeException;
import br.com.optimized.storage.FotoStorage;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private FotoStorage fotoStorage;
	
	public Page<Produto> filtrar(ProdutoFilter filtro, Pageable pageable){
		return produtoRepository.filtrar(filtro, pageable);
	}
	
	public List<ProdutoDTO> porSkuOuNome(String skuOuNome){
		return produtoRepository.porSkuOuNome(skuOuNome);
	}
	
	@Transactional
	public void salvar(Produto produto) {
		produtoRepository.save(produto);
	}
	
	@Transactional
	public void excluir(Produto produto) {
		try {
			String foto = produto.getFoto();
			produtoRepository.delete(produto);
			produtoRepository.flush();
			fotoStorage.excluir(foto);
		} catch (PersistenceException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar produto. Já foi usada em alguma venda.");
		}
	}
	
	public ValorItensEstoque valorItensEstoque() {
		return produtoRepository.valorItensEstoque();
	}

	public Produto findOne(Long id) {
		return produtoRepository.findOne(id);
	}
	
}
