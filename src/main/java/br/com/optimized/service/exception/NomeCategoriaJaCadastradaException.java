package br.com.optimized.service.exception;

public class NomeCategoriaJaCadastradaException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public NomeCategoriaJaCadastradaException(String message) {
		super(message);
	}

}
