package br.com.optimized.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.optimized.model.Cidade;
import br.com.optimized.model.Estado;
import br.com.optimized.repository.CidadeRepository;
import br.com.optimized.repository.filter.CidadeFilter;
import br.com.optimized.service.exception.ImpossivelExcluirEntidadeException;
import br.com.optimized.service.exception.NomeCidadeJaCadastradaException;

@Service
public class CidadeService {

	@Autowired
	private CidadeRepository cidadeRepository;
	
	public Page<Cidade> filtrar(CidadeFilter filtro, Pageable pageable){
		return cidadeRepository.filtrar(filtro, pageable);
	}
	
	@Transactional
	public void salvar(Cidade cidade) {
		Optional<Cidade> cidadeExistente = cidadeRepository.findByNomeAndEstado(cidade.getNome(), cidade.getEstado());
		if (cidadeExistente.isPresent()) {
			throw new NomeCidadeJaCadastradaException("Nome de cidade já cadastrado");
		}
		
		cidadeRepository.save(cidade);
	}
	
	public List<Cidade> findByEstadoCodigo(Long codigoEstado){
		return cidadeRepository.findByEstadoCodigo(codigoEstado);
	}

	public Optional<Cidade> findByNomeAndEstado(String nome, Estado estado){
		return cidadeRepository.findByNomeAndEstado(nome, estado);
	}
	
	@Transactional
	public void excluir(Cidade entidade) {
		try {
			cidadeRepository.delete(entidade);
			cidadeRepository.flush();
		} catch (PersistenceException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar Cidade.");
		}
	}
}
