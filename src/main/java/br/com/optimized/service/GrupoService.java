package br.com.optimized.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.optimized.model.Grupo;
import br.com.optimized.repository.GrupoRepository;

@Service
public class GrupoService {

	@Autowired
	private GrupoRepository grupoRepository;
	
	public List<Grupo> listar(){
		return grupoRepository.findAll();
	}
}
