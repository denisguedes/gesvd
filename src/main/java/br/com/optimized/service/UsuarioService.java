package br.com.optimized.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.optimized.model.Usuario;
import br.com.optimized.repository.UsuarioRepository;
import br.com.optimized.repository.filter.UsuarioFilter;
import br.com.optimized.service.exception.EmailUsuarioJaCadastradoException;
import br.com.optimized.service.exception.SenhaObrigatoriaUsuarioException;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public Optional<Usuario> findByEmail(String email){
		return usuarioRepository.findByEmail(email);
	}

	public List<Usuario> findByCodigoIn(Long[] codigos){
		return usuarioRepository.findByCodigoIn(codigos);
	}
	
	public Optional<Usuario> porEmailEAtivo(String email){
		return usuarioRepository.porEmailEAtivo(email);
	}
	
	public List<String> permissoes(Usuario usuario){
		return usuarioRepository.permissoes(usuario);
	}
	
	public Page<Usuario> filtrar(UsuarioFilter filtro, Pageable pageable){
		return usuarioRepository.filtrar(filtro, pageable);
	}
	
	public Usuario buscarComGrupos(Long codigo) {
		return usuarioRepository.buscarComGrupos(codigo);
	}
	
	@Transactional
	public void salvar(Usuario usuario) {
		Optional<Usuario> usuarioExistente = usuarioRepository.findByEmail(usuario.getEmail());
		if (usuarioExistente.isPresent() && !usuarioExistente.get().equals(usuario)) {
			throw new EmailUsuarioJaCadastradoException("E-mail já cadastrado");
		}
		
		if (usuario.isNovo() && StringUtils.isEmpty(usuario.getSenha())) {
			throw new SenhaObrigatoriaUsuarioException("Senha é obrigatória para novo usuário");
		}
		
		if (usuario.isNovo() || !StringUtils.isEmpty(usuario.getSenha())) {
			usuario.setSenha(this.passwordEncoder.encode(usuario.getSenha()));
		} else if (StringUtils.isEmpty(usuario.getSenha())) {
			usuario.setSenha(usuarioExistente.get().getSenha());
		}
		usuario.setConfirmacaoSenha(usuario.getSenha());
		
		if (!usuario.isNovo() && usuario.getAtivo() == null) {
			usuario.setAtivo(usuarioExistente.get().getAtivo());
		}
		
		usuarioRepository.save(usuario);
	}

	@Transactional
	public void alterarStatus(Long[] codigos, StatusUsuario statusUsuario) {
		statusUsuario.executar(codigos, usuarioRepository);
	}
	
}
