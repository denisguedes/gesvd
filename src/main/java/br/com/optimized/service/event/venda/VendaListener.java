package br.com.optimized.service.event.venda;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import br.com.optimized.model.ItemVenda;
import br.com.optimized.model.Produto;
import br.com.optimized.repository.ProdutoRepository;

@Component
public class VendaListener {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	@EventListener
	public void vendaEmitida(VendaEvent vendaEvent) {
		for (ItemVenda item : vendaEvent.getVenda().getItens()) {
			Produto produto = produtoRepository.findOne(item.getProduto().getCodigo());
			produto.setQuantidadeEstoque(produto.getQuantidadeEstoque() - item.getQuantidade());
			produtoRepository.save(produto);
		}
	}
	
}
